
describe('Password', () => {
  it('validates basic conditions', () => {
    const password = new Password('A123_456z')

    const isBasicPassword = password.isValidBasic()

    expect(isBasicPassword).toBe(true)
    expect(password.basicErrors()).toBe([])
  })

  it('does not validate basic conditions for less than eight characters', () => {
    const shortString = 'A23_46z'
    const password = new Password(shortString)

    const isBasicPassword = password.isValidBasic()

    expect(isBasicPassword).toBe(false)
    expect(password.basicErrors()).toBe(['too short'])
  })

  it('does not validate basic conditions for eight characters', () => {
    const shortString = 'A23_467z'
    const password = new Password(shortString)

    const isBasicPassword = password.isValidBasic()

    expect(isBasicPassword).toBe(false)
    expect(password.basicErrors()).toBe(['too short'])
  })

  it('does not validate basic conditions without a capital letter', () => {
    const noCapitalLetter = 'a123_456z'
    const password = new Password(noCapitalLetter)

    const isBasicPassword = password.isValidBasic()

    expect(isBasicPassword).toBe(false)
    expect(password.basicErrors()).toBe(['no capital letter'])
  })

  it('does not validate basic conditions without a lowercase letter', () => {
    const noLowercaseLetter = 'A123_456Z'
    const password = new Password(noLowercaseLetter)

    const isBasicPassword = password.isValidBasic()

    expect(isBasicPassword).toBe(false)
    expect(password.basicErrors()).toBe(['no lowercase letter'])
  })

  it('does not validate basic conditions without a number', () => {
    const noNumbers = 'ABcD_ABcD'
    const password = new Password(noNumbers)

    const isBasicPassword = password.isValidBasic()

    expect(isBasicPassword).toBe(false)
    expect(password.basicErrors()).toBe(['no numbers'])
  })

  it('does not validate basic conditions without an underscore', () => {
    const noUnderscore = 'A123X456z'
    const password = new Password(noUnderscore)

    const isBasicPassword = password.isValidBasic()

    expect(isBasicPassword).toBe(false)
    expect(password.basicErrors()).toBe(['no underscore'])
  })

  it('validates short conditions', () => {
    const password = new Password('A12345z')

    const isShortPassword = password.isValidShort()

    expect(isShortPassword).toBe(true)
  })

  it('does not validate short conditions with less than six characters', () => {
    const veryShortPassword = 'A145z'
    const password = new Password(veryShortPassword)

    const isShortPassword = password.isValidShort()

    expect(isShortPassword).toBe(false)
  })

  it('does not validate short conditions with six characters', () => {
    const veryShortPassword = 'A1456z'
    const password = new Password(veryShortPassword)

    const isShortPassword = password.isValidShort()

    expect(isShortPassword).toBe(false)
  })

  it('does not validate short conditions without a capital letter', () => {
    const noCapitalLetter = 'a12345z'
    const password = new Password(noCapitalLetter)

    const isShortPassword = password.isValidShort()

    expect(isShortPassword).toBe(false)
  })

  it('does not validate short conditions without a lowercase letter', () => {
    const noLowercaseLetter = 'A12345Z'
    const password = new Password(noLowercaseLetter)

    const isShortPassword = password.isValidShort()

    expect(isShortPassword).toBe(false)
  })

  it('does not validate short conditions without a number', () => {
    const noNumbers = 'Aasdasdasz'
    const password = new Password(noNumbers)

    const isShortPassword = password.isValidShort()

    expect(isShortPassword).toBe(false)
  })
})

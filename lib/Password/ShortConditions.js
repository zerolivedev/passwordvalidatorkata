
const ShortConditions = class {
  static meets(password) {
    const minimumCharacter = 6

    return password.hasMoreThan(minimumCharacter) &&
      password.hasCapitalLetter() &&
      password.hasLowerCaseLetter() &&
      password.hasANumber()
  }
}

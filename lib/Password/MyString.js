
const MyString = class {
  constructor(value) {
    this.value = value
  }

  hasMoreThan(quantityOfCharacter) {
    return (this.value.length > quantityOfCharacter)
  }

  hasCapitalLetter() {
    return (this.value.toLowerCase() !== this.value)
  }

  hasLowerCaseLetter() {
    return (this.value.toUpperCase() !== this.value)
  }

  hasANumber() {
    let hasANumber = false

    for (const character of this.value) {
      const integer = parseInt(character)
      const isAInteger = (!isNaN(integer))

      if (isAInteger) {
        hasANumber = true
      }
    }

    return hasANumber
  }

  hasAnUnderscore() {
    let hasAnUnderscore = false

    for (const character of this.value) {
      if (character === '_') {
        hasAnUnderscore = true
      }
    }

    return hasAnUnderscore
  }
}


const BasicConditions = class {
  static errors(password) {
    const errors = Errors.empty()

    if (!this._hasMinimumCharacters(password)) {
      errors.addTooShort()
    }

    if (!this._hasCapitalLetter(password)) {
      errors.addNoCapitalLetter()
    }

    if(!this._hasLowerCaseLetter(password)) {
      errors.addNoLowerCaseLetter()
    }

    if(!this._hasANumber(password)) {
      errors.addNoNumbers()
    }

    if(!this._hasAnUnderscore(password)) {
      errors.addNoUnderscore()
    }

    return errors.toList()
  }

  static match(password) {
    return !this._hasErrors(password)
  }

  static _hasErrors(password) {
    return (this.errors(password).length !== 0)
  }

  static _hasMinimumCharacters(password) {
    const minimumCharacter = 8

    return password.hasMoreThan(minimumCharacter)
  }

  static _hasCapitalLetter(password) {
    return password.hasCapitalLetter()
  }

  static _hasLowerCaseLetter(password) {
    return password.hasLowerCaseLetter()
  }

  static _hasANumber(password) {
    return password.hasANumber()
  }

  static _hasAnUnderscore(password) {
    return password.hasAnUnderscore()
  }
}

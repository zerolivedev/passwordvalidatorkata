
const Errors = class {
  static empty() {
    return new Errors()
  }

  constructor() {
    this.errors = []
  }

  addTooShort() {
    this.errors.push('too short')
  }

  addNoCapitalLetter() {
    this.errors.push('no capital letter')
  }

  addNoLowerCaseLetter() {
    this.errors.push('no lowercase letter')
  }

  addNoNumbers() {
    this.errors.push('no numbers')
  }

  addNoUnderscore() {
    this.errors.push('no underscore')
  }

  toList() {
    return this.errors
  }
}

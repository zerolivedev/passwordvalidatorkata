
const Password = class {
  constructor(password) {
    this.password = new MyString(password)
  }

  basicErrors() {
    return BasicConditions.errors(this.password)
  }

  isValidBasic() {
    return BasicConditions.match(this.password)
  }

  isValidShort() {
    return ShortConditions.meets(this.password)
  }
}
